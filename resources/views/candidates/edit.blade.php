@extends('layouts.app')
@section('title', 'Edit Candidate')
@section('content')
<h1>Edit candidate</h1>
<form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
    @method('PATCH')
    @csrf
    <div class="form-group">
        <label for = "name">Candidate name</label>
        <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
    </div>
    <div class="form-group">
        <label for = "name">Candidate email</label>
        <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
    </div>
    <div>
        <input type = "submit" class="btn btn-primary" name = "submit" value = "Update Candidate">
    </div>
</form>  
@endsection

